/****************************************************
 * Description: ServiceImpl for t_sys_xfile
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-05-04 RY Create File
**************************************************/

package cn.com.ry.framework.application.meteor.sys.xfile.service.impl;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.framework.service.XjjServiceSupport;
import cn.com.ry.framework.application.meteor.sys.xfile.dao.XfileDao;
import cn.com.ry.framework.application.meteor.sys.xfile.entity.XfileEntity;
import cn.com.ry.framework.application.meteor.sys.xfile.service.XfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XfileServiceImpl extends XjjServiceSupport<XfileEntity> implements XfileService {

	@Autowired
	private XfileDao xfileDao;

	@Override
	public XjjDAO<XfileEntity> getDao() {

		return xfileDao;
	}
}

package cn.com.ry.framework.application.meteor.sys.code;

import freemarker.template.Configuration;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class GCConfig {


    private static Configuration cfg = null;
    private static GCConfig instance = null;
    private static Properties properties = CodeFilePropertyConfigLoader.load();
    //生成代码的保存地址
    public static String DIR_BASE = ((String) properties.get("code.dir.base")).endsWith("/") ? (String) properties.get("code.dir.base") : (String) properties.get("code.dir.base") + "/";
    //生成的java源码的子目录
    private String DIR_SRC = DIR_BASE + "src/main/java/";
    //生成的页面文件的子目录
    private String DIR_WEB = DIR_BASE + "src/main/resources/templates/";
    //生成的页面文件的子目录
    private String DIR_MAPPER = DIR_BASE + "src/main/resources/mapper/";
    //生成的execl目录
    private String DIR_IMPORT = DIR_BASE + "src/main/resources/static/import-template/";
    //生成代码的作者姓名
    private String INFO_AUTHOR = (String) properties.get("info.author");
    //生成代码的公司名称
    private String INFO_COMPANY = (String) properties.get("info.company");
    //代码的版本
    private String INFO_VERSION = (String) properties.get("info.version");
    //工程路径
    private String INFO_URLPATH = "/";
    //页面模板的名字，默认defaults
    public static String DIR_TEMPLATE_VIEW = "ace";

    private GCConfig() {
    }

    public static GCConfig getInstance() {
        if (null == instance) {
            instance = new GCConfig();
        }
        return instance;
    }

    public Configuration getConfiguration() {

        if (null == cfg) {
            cfg = new Configuration();
            cfg.setClassForTemplateLoading(this.getClass(),
                    "template");
            try {
                String classNames = this.getClass().getPackage().getName().replace(".", "/");
                String DIR_TEMPLATE = DIR_SRC + classNames + "/template";
                File file = new File(DIR_TEMPLATE);
                cfg.setDirectoryForTemplateLoading(file);
            } catch (IOException e) {
                System.out.println("如果生成代码报错，修改GCConfig.java的44行的模版文件路径");
            }

            cfg.setDefaultEncoding("UTF-8");
        }

        Configuration cfgDebug = cfg;
        System.out.println("========================cfgDebug");
        System.out.println(cfgDebug);
        return cfg;
    }

    public String getBaseDir() {
        return DIR_BASE;
    }

    public String getSrcDir() {
        return DIR_SRC;
    }

    public String getViewTemplateDir() {
        if (DIR_TEMPLATE_VIEW == null || DIR_TEMPLATE_VIEW.equals("")) {
            return "defaults/";
        }
        if (DIR_TEMPLATE_VIEW.endsWith("/")) {
            return DIR_TEMPLATE_VIEW;
        }
        return DIR_TEMPLATE_VIEW + "/";
    }

    public String getWebDir() {
        return DIR_WEB;
    }

    public String getMapperDir() {
        return DIR_MAPPER;
    }

    public String getImportDir() {
        return DIR_IMPORT;
    }

    public String getAuthor() {
        return INFO_AUTHOR;
    }

    public String getCompany() {
        return INFO_COMPANY;
    }

    public String getVersion() {
        return INFO_VERSION;
    }

    public String getUrlPath() {
        return INFO_URLPATH;
    }
}

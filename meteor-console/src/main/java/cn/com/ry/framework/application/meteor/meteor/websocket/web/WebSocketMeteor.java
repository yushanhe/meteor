package cn.com.ry.framework.application.meteor.meteor.websocket.web;

import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.spring.SpringBeanLoader;
import cn.com.ry.framework.application.meteor.framework.ssh.SshSessionEntity;
import cn.com.ry.framework.application.meteor.framework.ssh.SshSessionFactory;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoEntity;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.service.MachineinfoService;
import cn.com.ry.framework.application.meteor.meteor.rsa.entity.RsaEntity;
import cn.com.ry.framework.application.meteor.meteor.rsa.service.RsaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

@ServerEndpoint("/websocketmeteor/{sid}/{machineId}")
@Component
public class WebSocketMeteor {
    private Logger log = LoggerFactory.getLogger(WebSocketMeteor.class);

    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;
    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static CopyOnWriteArraySet<WebSocketMeteor> webSocketSet = new CopyOnWriteArraySet<WebSocketMeteor>();

    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    //ssh
    private SshSessionEntity sshSessionEntity;

    //接收sid
    private String sid = "";

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("sid") String sid, @PathParam("machineId") Long machineId) {
        this.session = session;
        webSocketSet.add(this);     //加入set中
        addOnlineCount();           //在线数加1
        log.info("有新窗口开始监听:" + sid + ",当前在线人数为" + getOnlineCount());
        this.sid = sid;
        try {
            sshSessionEntity = null;
            //创建ssh连接
            if (machineId != null) {
                XJJParameter xjjParameter = new XJJParameter();
                xjjParameter.addQuery("query.id@eq@l", machineId);
                xjjParameter.addQuery("query.status@eq@s", "valid");
                MachineinfoService machineinfoService = SpringBeanLoader.getBean(MachineinfoService.class);
                List<MachineinfoEntity> machineinfoEntityList = machineinfoService.findList(xjjParameter);
                if (machineinfoEntityList != null && machineinfoEntityList.size() > 0) {
                    MachineinfoEntity machineinfoEntity = machineinfoEntityList.get(0);
                    String hostname = machineinfoEntity.getHostname();
                    Integer port = machineinfoEntity.getPort();
                    if (machineinfoEntity.getLoginType() != null && machineinfoEntity.getLoginType().equals("rsa")) {
                        xjjParameter = new XJJParameter();
                        xjjParameter.addQuery("query.id@eq@l", machineinfoEntity.getRsaId().longValue());
                        xjjParameter.addQuery("query.status@eq@s", "valid");
                        RsaService rsaService = SpringBeanLoader.getBean(RsaService.class);
                        List<RsaEntity> rsaEntityList = rsaService.findList(xjjParameter);
                        if (rsaEntityList != null && rsaEntityList.size() > 0) {
                            RsaEntity rsaEntity = rsaEntityList.get(0);
                            String username = rsaEntity.getRsaUsername();
                            String password = rsaEntity.getRsaPassword();
                            String rsaValue = rsaEntity.getRsaValue();
                            sshSessionEntity = SshSessionFactory.createSession(hostname, username, password, port, rsaValue.toCharArray(), sid, session);
                        } else {
                            throw new ValidationException("该服务器不存在，请检查是否禁用");
                        }
                    } else {
                        String username = machineinfoEntity.getUsername();
                        String password = machineinfoEntity.getPassword();
                        sshSessionEntity = SshSessionFactory.createSession(hostname, username, password, port, sid, session);
                    }
                } else {
                    throw new ValidationException("该服务器不存在，请检查是否禁用");
                }
            } else {
                throw new ValidationException("服务器ID不能为空");
            }
            sendMessage("");
        } catch (IOException e) {
            throw new ValidationException(e.getMessage());
        } catch (ValidationException e) {
            throw new ValidationException(e.getMessage());
        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);  //从set中删除
        subOnlineCount();           //在线数减1
        SshSessionFactory.closeSession(sshSessionEntity);
        log.info("有一连接关闭！当前在线人数为" + getOnlineCount());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        //群发消息
        for (WebSocketMeteor item : webSocketSet) {
            try {
                String tmp_sid = item.sid;
                if (tmp_sid.equals(sid)) {
                    item.sendMessage(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 错误信息
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 实现服务器主动推送
     */
    public void sendMessage(String message) throws IOException {
        sshSessionEntity.write(message);
    }

    /**
     * 群发自定义消息
     */
    public static void sendInfo(String message, @PathParam("sid") String sid) throws IOException {
        for (WebSocketMeteor item : webSocketSet) {
            try {
                //这里可以设定只推送给这个sid的，为null则全部推送
                if (sid == null) {
                    item.sendMessage(message);
                } else if (item.sid.equals(sid)) {
                    item.sendMessage(message);
                }
            } catch (IOException e) {
                continue;
            }
        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketMeteor.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketMeteor.onlineCount--;
    }
}

/****************************************************
 * Description: Controller for 数据字典
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
	*  2019-08-16 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sys.dict.web;

import cn.com.ry.framework.application.meteor.common.DictConstants;
import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.security.annotations.*;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.framework.web.support.Pagination;
import cn.com.ry.framework.application.meteor.framework.web.support.QueryParameter;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import cn.com.ry.framework.application.meteor.sys.dict.entity.DictEntity;
import cn.com.ry.framework.application.meteor.sys.dict.service.DictService;
import cn.com.ry.framework.application.meteor.sys.dictgroup.service.DictgroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/sys/dict")
public class DictController extends SpringControllerSupport{
	@Autowired
	private DictService dictService;
	@Autowired
	private DictgroupService dictgroupService;


	@SecPrivilege(title="数据字典管理")
	@RequestMapping(value = "/index")
	public String index(Model model) {
		String page = this.getViewPath("index");
		model.addAttribute("groupList",dictgroupService.findAll());
		return page;
	}

	@SecList
	@RequestMapping(value = "/list")
	public String list(Model model,
			@QueryParameter XJJParameter query,
			@ModelAttribute("page") Pagination page
			) {
		page = dictService.findPage(query,page);
		return getViewPath("list");
	}

	@SecCreate
	@RequestMapping("/input")
	public String create(@ModelAttribute("dict") DictEntity dict,Model model){
		//获取数据字典组列表
		model.addAttribute("groupList",dictgroupService.findAll());
		return getViewPath("input");
	}

	@SecEdit
	@RequestMapping("/input/{id}")
	public String edit(@PathVariable("id") Long id, Model model){
		DictEntity dict = dictService.getById(id);
		model.addAttribute("dict",dict);
		//获取数据字典组列表
		model.addAttribute("groupList",dictgroupService.findAll());
		return getViewPath("input");
	}

	@SecCreate
	@SecEdit
	@RequestMapping("/save")
	public @ResponseBody XjjJson save(@ModelAttribute DictEntity dict){

		validateSave(dict);
		if(dict.isNew())
		{
			//dict.setCreateDate(new Date());
			dictService.save(dict);
		}else
		{
			dictService.update(dict);
		}
		//刷新缓存
		DictConstants.init();
		return XjjJson.success("保存成功");
	}


	/**
	 * 数据校验
	 **/
	private void validateSave(DictEntity dict){
		//必填项校验
		// 判断编码是否为空
		if(null==dict.getCode()){
			throw new ValidationException("校验失败，编码不能为空！");
		}
		// 判断属组是否为空
		if(null==dict.getGroupCode()){
			throw new ValidationException("校验失败，属组不能为空！");
		}
		// 判断名称是否为空
		if(null==dict.getName()){
			throw new ValidationException("校验失败，名称不能为空！");
		}
		// 判断状态是否为空
		if(null==dict.getStatus()){
			throw new ValidationException("校验失败，状态不能为空！");
		}
	}

	@SecDelete
	@RequestMapping("/delete/{id}")
	public @ResponseBody XjjJson delete(@PathVariable("id") Long id){
		dictService.delete(id);
		DictConstants.init();
		return XjjJson.success("成功删除1条");
	}
	@SecDelete
	@RequestMapping("/delete")
	public @ResponseBody XjjJson delete(@RequestParam("ids") Long[] ids){
		if(ids == null || ids.length == 0){
			return XjjJson.error("没有选择删除记录");
		}
		for(Long id : ids){
			dictService.delete(id);
			DictConstants.init();
		}
		return XjjJson.success("成功删除"+ids.length+"条");
	}
}


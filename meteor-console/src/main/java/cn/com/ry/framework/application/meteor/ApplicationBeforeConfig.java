package cn.com.ry.framework.application.meteor;

import cn.com.ry.framework.application.meteor.framework.spring.SpringTool;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

@Configuration
public class ApplicationBeforeConfig implements BeanPostProcessor {
    /**
     * 初始化h2数据库
     */
    public synchronized static void initDB() throws IOException {
        String projectPath = SpringTool.getProjectPath();
        String dbHomePath = projectPath + File.separator + "meteordb";
        File fileDbHomePath = new File(dbHomePath);
        if (!fileDbHomePath.exists()) {
            //判断是否存在h2文件
            String[] fileTags = {"mv", "trace"};
            byte[] byteDatas = null;
            for (String fileTag : fileTags) {
                byteDatas = SpringTool.getResourceBytes("db/h2/meteor." + fileTag + ".db");
                SpringTool.saveFile(byteDatas, dbHomePath, "meteor." + fileTag + ".db");
            }
        }
    }

    @PostConstruct
    public void test() {
        try {
            System.out.println("【开始】初始化数据库");
            initDB();
            System.out.println("【结束】初始化数据库");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}


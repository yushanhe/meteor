<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>
<@content>
    <@query id=tabId>
        <div class="row">
            <input type="hidden" id="timetunnel-className">
            <input type="hidden" id="timetunnel-method">
            <@querygroup title='监控次数'>
                <input type="text" id="timetunnel-count" value="50" class="form-control input-sm" placeholder="最大30秒"
                       check-type='required number'>
            </@querygroup>
            <@button type="info" onclick="sendTimetunnelCmd()">重试</@button>
        </div>
    <#--重试/查看请求-->
        <div class="row">
            <@querygroup title='请求INDEX'>
                <input type="text" id="timetunnel-index" class="form-control input-sm" placeholder="index">
            </@querygroup>
            <@button type="info"  onclick="detail()">查看请求详细</@button>
            <@button type="info"  onclick="sendRetry()">重发请求</@button>
        </div>
    </@query>

</@content>

<div class="row">
    <div class="container-fluid px-0">
        <div class="col px-0" id="timetunnel-console-card">
            <div id="timetunnel-console"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var timetunnel_ws = null;

    <#--XJJ.initInput({"id":"${tabId}"});-->

    initTimetunnelConsole();

    function initTimetunnelConsole() {
        var arthas_ip = '${meteor_param.arthasIp}';
        var arthas_port = '${meteor_param.arthasPort}';
        var arthas_agentId = '${meteor_param.arthasAgentId}';
        if (arthas_ip == '') {
            XJJ.msger('机器IP不能为空,请在[meteor]-[全局参数设置]中进行配置');
            return;
        } else if (arthas_port == '') {
            XJJ.msger('PORT不能为空');
            return;
        }

        //获取websocket连接
        var path = 'ws://' + arthas_ip + ':' + arthas_port + '/ws';
        if (arthas_agentId != 'null' & arthas_agentId != "" && arthas_agentId != null) {
            path = path + '?method=connectArthas&id=' + arthas_agentId;
        }
        timetunnel_ws = initArthasConsole("timetunnel-console", path, "meteor_timetunnel",true);
    }

    function brake(){
        timetunnel_ws.send(JSON.stringify({action: 'read', data: ""}));
    }

    function detail() {
        brake();
        var timetunnelIndex = $("#timetunnel-index").val();
        var cmd = "tt -i " + timetunnelIndex;
        timetunnel_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));
    }

    function sendRetry() {
        brake();
        var timetunnelIndex = $("#timetunnel-index").val();
        var cmd = "tt -i " + timetunnelIndex + " -p";
        timetunnel_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));
    }


    function sendTimetunnelCmd(className, method) {
        if (className != null) {
            $("#timetunnel-className").val(className);
        }
        if (method != null) {
            $("#timetunnel-method").val(method);
        }

        var className_cmd = $("#timetunnel-className").val();
        var method_cmd = $("#timetunnel-method").val();
        var count = $("#timetunnel-count").val();
        if (count > 100) {
            count = 100;
        }
        var cmd = "tt -t -n " + count+" " + className_cmd + " " + method_cmd + "";
        timetunnel_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));
    }
</script>
